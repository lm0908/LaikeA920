package com.laike.pay.base;

import android.os.Environment;

public class Constants {
    /**
     * 存用户配置info的key
     */
    public static final String APP_INFO_SP_NAME = "app_info_sp_name";
    public static final String APP_ID = "appid";
    public static final String APP_SECRET = "app_secret";
    public static final String CODE = "code";
    public static final String WORK_MEN_ID = "work_men_id";
    public static final String SPEAK_PLAY = "speak_play";
    public static final String IS_FRONT_CAMERA = "is_front_camera";

    /**
     * 存商家info
     */
    public static final String SHOP_INFO_SP_NAME = "shop_info_sp_name";
    public static final String SHOP_NAME = "shop_name";
    public static final String BANK_ACOUNT_NAME = "bank_acount_name";
    public static final String BANK_NUMBER = "bank_number";
    public static final String URL = "url";

    /**
     * 版本
     */
    public static final String NEW_VERSION_NAME = "bf_duolaibei";
    public static final String NEW_VERSION_SECRET = "w34fkj2paezx67tyuvbnmgidc895ls1rhqo";
    public static final String DOWNLOAD_DIR
            = Environment.getExternalStorageDirectory().getPath() +
            "/Android/data/com.duolaibei.pay/apk/";
}
