package com.laike.pay.base;

import android.Manifest;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.laike.pay.R;
import com.laike.pay.activity.BrushCard2Activity;
import com.laike.pay.activity.BrushCardActivity;
import com.laike.pay.utils.NetUtil;
import com.laike.pay.utils.ToastUtils;
import com.pax.gl.IGL;
import com.pax.ippi.dal.interfaces.IDal;
import com.pax.ippi.emv.interfaces.IEmv;
import com.pax.ippi.impl.NeptuneUser;
import com.pax.pay.service.aidl.PayHelper;

import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout back; //返回
    TextView text;//文本
    protected TextView serach;//右边文本
    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE};
    private BaseActivity mThis;

    public NeptuneUser neptuneUser;
    public static IDal dal;
    public static IGL gl;
    public static IEmv emv;

    public PayHelper payHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mThis = this;
        //添加activity到stack中
        MyApplication.getApp().addActivity(mThis);
        setContentView(getContentViewResId());
        ButterKnife.bind(this);
        initToolbar();
        if (EasyPermissions.hasPermissions(this, perms)) {
            intiViews();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.sdcard_permissions_tips), 1, perms);
        }
        onNetChange();
        neptuneUser = NeptuneUser.getInstance(this);
        dal = neptuneUser.getService().getDal();
        gl = neptuneUser.getService().getGl();
        emv = neptuneUser.getService().getEmv();
        bindservice();//绑定服务
    }
    /**
     * 绑定百富服务
     */
    private void bindservice() {
        Intent intent = new Intent();
        intent.setAction("com.pax.pay.SERVICE");
        try {
            bindService(intent, conn, Service.BIND_AUTO_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private ServiceConnection conn = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            payHelper = PayHelper.Stub.asInterface(arg1);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bindservice();//绑定服务
        }
    };

    @Override
    protected void onResume() {
        try {
            neptuneUser.register();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        try {
            neptuneUser.unRegister();
            unbindService(conn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    /**
     * 继承类实现，返回布局文件 布局文件必须包含公共的titlebar
     *
     * @return
     */
    public abstract int getContentViewResId();

    public abstract void intiViews();

    /**
     * 设置text
     *
     * @return
     */
    public abstract String setTitle();


    protected void initToolbar() {
        if (mThis instanceof BrushCardActivity
                || mThis instanceof BrushCard2Activity
                ) {
        } else {
            back = (RelativeLayout) findViewById(R.id.back);
            text = (TextView) findViewById(R.id.text);
            serach = (TextView) findViewById(R.id.serach);
            if (null == back || null == text) {
                return;
            }
            if (null != setTitle()) {
                text.setText(setTitle());
            }
            back.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.back)
            this.finish();
    }

    public boolean onNetChange() {
        if (NetUtil.getNetworkState(this) == NetUtil.NETWORN_NONE) {
            ToastUtils.showToast(this, "手机没有网络连接", 3000);
            return false;
        }
        return true;
    }

}
