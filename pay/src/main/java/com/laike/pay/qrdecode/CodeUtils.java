package com.laike.pay.qrdecode;

/**
 * Created by xlm on 2018\5\30 0030
 */

public class CodeUtils {
    public static final String RESULT_TYPE = "result_type";
    public static final String RESULT_STRING = "result_string";
    public static final int RESULT_SUCCESS = 1;
    public static final int RESULT_FAILED = 2;
}
