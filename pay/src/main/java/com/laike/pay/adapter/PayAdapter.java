package com.laike.pay.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.laike.pay.R;
import com.laike.pay.bean.PayOrder;
import com.laike.pay.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PayAdapter extends BaseAdapter {

    private Context context;

    private List<PayOrder> listItems = new ArrayList<>();

    public PayAdapter() {

    }

    public PayAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return listItems == null ? 0 : listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return listItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.detail_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        bindView(holder, i);
        return view;
    }

    private void bindView(ViewHolder holder, int poi) {
        PayOrder detail = (PayOrder) getItem(poi);
        holder.paystyleTt.setText(detail.getPay_name());
        holder.moneyTt.setText(context.getString(R.string.price, detail.getTotal_amount()));
        holder.paytimeTt.setText(TimeUtils.stampToDate(detail.getPaytime() + "000", "yyyy-MM-dd\tHH:mm:ss"));
        if (detail.getStatus().equals("1")) {
            holder.payresultTt.setText(R.string.deal_succ);
            holder.payresultTt.setTextColor((ContextCompat.getColor(context, R.color.homepage_text_color)));
        } else if (detail.getStatus().equals("-1")) {
            holder.payresultTt.setText(R.string.refunded);
            holder.payresultTt.setTextColor((ContextCompat.getColor(context, R.color.refund_color)));
        } else if (detail.getStatus().equals("0")) {
            holder.payresultTt.setText(R.string.no_pay);
        }
    }

    static class ViewHolder {
        @Bind(R.id.paystyle_tt)
        TextView paystyleTt;
        @Bind(R.id.money_tt)
        TextView moneyTt;
        @Bind(R.id.paytime_tt)
        TextView paytimeTt;
        @Bind(R.id.payresult_tt)
        TextView payresultTt;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void setData(List<PayOrder> payOrders) {
        if (null == payOrders) {
            return;
        }
        listItems.addAll(payOrders);
        notifyDataSetChanged();

    }
}
