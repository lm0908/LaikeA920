package com.laike.pay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.laike.pay.R;

import butterknife.Bind;
import butterknife.ButterKnife;


public class BrushAdapter extends BaseAdapter {
    private Context context = null;
    public BrushAdapter(Context context) {
        this.context = context;
    }

    //10代表"."；11 代表"删除健"
    private int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 11};

    @Override
    public int getCount() {
        return array.length;
    }

    @Override
    public Object getItem(int i) {
        return array[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.gridview_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        bindView(holder, i);
        return view;
    }

    private void bindView(ViewHolder holder, int position) {
        int poi = array[position];
        if (poi == 10) {
            holder.number.setText(".");
        } else if (poi == 11) {
            holder.number.setText("");
            holder.img.setVisibility(View.VISIBLE);
        } else {
            holder.number.setText(poi + "");
        }
    }

    static class ViewHolder {
        @Bind(R.id.number)
        TextView number;
        @Bind(R.id.img)
        ImageView img;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
