package com.laike.pay.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.laike.pay.R;
import com.laike.pay.base.BaseActivity;
import com.laike.pay.qrdecode.CodeUtils;
import com.laike.pay.qrdecode.MipcaActivityCapture;

import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

public class SweepManageActivity extends BaseActivity {

    private LinearLayout detailLayout;//交易明细
    private LinearLayout reimburseLayout;//扫码退款
    private LinearLayout checklistLayout;//扫码查单
    private LinearLayout settlementLayout;//结算
    private final int REQUEST_CODE = 100;
    private final int CAMERA_CODE = 0;

    @Override
    public int getContentViewResId() {
        return R.layout.activity_sweepmg;
    }

    @Override
    public void intiViews() {
        detailLayout = (LinearLayout) findViewById(R.id.detail_layout);
        reimburseLayout = (LinearLayout) findViewById(R.id.reimburse_layout);
        checklistLayout = (LinearLayout) findViewById(R.id.checklist_layout);
        settlementLayout = (LinearLayout) findViewById(R.id.settlement_layout);
        detailLayout.setOnClickListener(this);
        reimburseLayout.setOnClickListener(this);
        checklistLayout.setOnClickListener(this);
        settlementLayout.setOnClickListener(this);
    }

    @Override
    public String setTitle() {
        return "扫码管理";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        super.onAttachedToWindow();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Intent intent = new Intent(this, CollectMoneyActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        Intent intent = null;
        switch (id) {
            case R.id.detail_layout:
                intent = new Intent(this, TransactionActivity.class);
                break;
            case R.id.reimburse_layout:
                String[] perms = {Manifest.permission.CAMERA};
                if (EasyPermissions.hasPermissions(this, perms)) {//检查是否获取该权限
                    Intent qrIntent = new Intent(this, MipcaActivityCapture.class);
                    qrIntent.putExtra("type", 300);
                    qrIntent.putExtra("title", getString(R.string.sweep_back_money));
                    startActivityForResult(qrIntent, REQUEST_CODE);
                } else {
                    EasyPermissions.requestPermissions(this, getString(R.string.get_permissions_tips), CAMERA_CODE, perms);
                }
                break;
            case R.id.checklist_layout:
                String[] perms1 = {Manifest.permission.CAMERA};
                if (EasyPermissions.hasPermissions(this, perms1)) {//检查是否获取该权限
                    Intent qrIntent = new Intent(this, MipcaActivityCapture.class);
                    qrIntent.putExtra("title", getString(R.string.sweep_select_deal));
                    qrIntent.putExtra("type", 100);
                    startActivity(qrIntent);
                } else {
                    EasyPermissions.requestPermissions(this, getString(R.string.get_permissions_tips), CAMERA_CODE, perms1);
                }
                break;
            case R.id.settlement_layout:
                Intent intent1 = new Intent(this, SweepGatheringActivity.class);
                startActivity(intent1);
                break;
        }
        if (null != intent) {
            startActivity(intent);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            //处理扫描结果（在界面上显示）
            if (null != data) {
                Bundle bundle = data.getExtras();
                if (bundle == null) {
                    return;
                }
                if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
                    String result = bundle.getString(CodeUtils.RESULT_STRING);
                } else if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_FAILED) {
                    Toast.makeText(this, "解析二维码失败", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
