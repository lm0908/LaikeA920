package com.laike.pay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laike.pay.R;
import com.laike.pay.baifuprint.PrinterUtil;
import com.laike.pay.base.BaseActivity;
import com.laike.pay.bean.PayOrder;
import com.laike.pay.utils.TimeUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DealDetailsActivity extends BaseActivity {
    @Bind(R.id.price)
    TextView price;//金额
    @Bind(R.id.business)
    TextView business;//商家
    @Bind(R.id.pay_type)
    TextView payType;//支付类型
    @Bind(R.id.pay_time)
    TextView payTime;//支付时间
    @Bind(R.id.deal_num)
    TextView dealNum;//交易号
    @Bind(R.id.deal_status)
    TextView dealStatus;//交易状态
    @Bind(R.id.print)
    TextView print;//打印
    @Bind(R.id.describe_layout)
    LinearLayout descLayout;//打印
    @Bind(R.id.certificate_layout)
    LinearLayout certificateLayout;
    @Bind(R.id.certificate_number)
    TextView certificateNumber;
    private PayOrder order = null;
    private int type = 0;
    private int indexType = 0;

    @Override
    public int getContentViewResId() {
        return R.layout.activity_deal_details;
    }

    @Override
    public void intiViews() {
        Intent intent = getIntent();
        if (null != intent) {
            order = intent.getParcelableExtra("order");
            type = intent.getIntExtra("type", 0);
            indexType = intent.getIntExtra("index", 0);
        }
        if (indexType == 1) {//刷卡
            certificateLayout.setVisibility(View.VISIBLE);
        } else {
            certificateLayout.setVisibility(View.GONE);
        }
        print.setOnClickListener(this);
        bindView();
    }

    @Override
    public String setTitle() {
        return getString(R.string.deal_details_text);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        super.onAttachedToWindow();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Intent intent = new Intent(this, CollectMoneyActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        int id = view.getId();
        if (id == R.id.print) {
            //打印
            new Thread(new Runnable() {
                @Override
                public void run() {
                    PrinterUtil printer = new PrinterUtil(DealDetailsActivity.this);
                    try {
                        if (indexType == 1) {
                            printer.printDealDetail(order, 1);
                        } else {
                            printer.printDealDetail(order, 0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    /**
     * 初始化打印
     */
    private void bindView() {
        certificateNumber.setText(order.getTrade_no());
        price.setText(getString(R.string.price, order.getTotal_amount()));
        business.setText(order.getBody());
        payType.setText(order.getPay_name());
        dealNum.setText(order.getOut_trade_no());
        payTime.setText(TimeUtils.stampToDate(order.getPaytime() + "000"));
        if (order.getStatus().equals("1")) {
            dealStatus.setText(R.string.deal_succ);
            print.setVisibility(View.VISIBLE);
            price.setTextColor(ContextCompat.getColor(this, R.color.common_title_color));
        } else if (order.getStatus().equals("-1")) {
            dealStatus.setText(R.string.refunded);
            print.setVisibility(View.GONE);
            descLayout.setVisibility(View.GONE);
            price.setTextColor(ContextCompat.getColor(this, R.color.refund_color));
            dealStatus.setTextColor(ContextCompat.getColor(this, R.color.refund_color));
        } else if (order.getStatus().equals("0")) {
            dealStatus.setText(R.string.no_pay);
        }
        if (type == 200) {
            for (int i = 0; i < 2; i++) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        PrinterUtil printer = new PrinterUtil(DealDetailsActivity.this);
                        try {
                            if (indexType == 1) {
                                printer.printDealDetail(order, 1);
                            } else {
                                printer.printDealDetail(order, 0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
