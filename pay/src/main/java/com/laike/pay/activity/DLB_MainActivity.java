package com.laike.pay.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.laike.pay.R;
import com.laike.pay.base.BaseActivity;
import com.laike.pay.base.Constants;
import com.laike.pay.bean.PayOrder;
import com.laike.pay.greendao.PayOrderDao;
import com.laike.pay.qrdecode.CodeUtils;
import com.laike.pay.qrdecode.MipcaActivityCapture;
import com.laike.pay.utils.DateUtils;
import com.laike.pay.utils.GreenDaoManager;
import com.laike.pay.utils.SharedPreferencesUtils;
import com.laike.pay.utils.TimeUtils;
import com.laike.pay.utils.ToastUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * APPID保存设置
 */
public class DLB_MainActivity extends BaseActivity {

    @Bind(R.id.appid)
    EditText appid;
    @Bind(R.id.secret_key)
    EditText secretKey;
    @Bind(R.id.terminal_number)
    EditText terminalNumber;
    @Bind(R.id.speech_check)
    AppCompatCheckBox speechCheck;
    @Bind(R.id.save_setting)
    TextView saveSetting;
    @Bind(R.id.workmen_id)
    EditText workmenId;
    @Bind(R.id.front_check)
    AppCompatCheckBox frontCheck;
    @Bind(R.id.front_check_layout)
    LinearLayout frontCheckLayout;
    @Bind(R.id.back)
    RelativeLayout mBack;
    private final int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PayOrderDao payOrderDao = GreenDaoManager.getInstance().getNewSession().getPayOrderDao();
        List<PayOrder> orders = payOrderDao.queryBuilder().list();
        for (PayOrder order : orders) {
            int days = DateUtils.getBetweenDay(TimeUtils.strToDate(order.getPaytime() + "000", "yyyy-MM-dd HH:mm:ss"), new Date());
            // 大于手机当前时间15天的记录删除
            if (days > 15)
                payOrderDao.deleteByKey(order.getId());
        }
        if (isShowView()) {
            Intent intent = new Intent(this, HomepageActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onAttachedToWindow() {
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        super.onAttachedToWindow();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Intent intent = new Intent(this, DLB_MainActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected boolean isShowView() {
        String appid = SharedPreferencesUtils.getAppid();
        return null != appid && appid.length() > 0;
    }

    @Override
    public int getContentViewResId() {
        return R.layout.activity_main;
    }

    @Override
    public void intiViews() {
        saveSetting.setOnClickListener(this);
        appid.setText(SharedPreferencesUtils.getAppid());
        secretKey.setText(SharedPreferencesUtils.getAppSecret());
        terminalNumber.setText(SharedPreferencesUtils.getTerminalNumber());
        speechCheck.setChecked(SharedPreferencesUtils.getPlaySpeak().equals("1"));
        workmenId.setText(SharedPreferencesUtils.getWorkMenId());
        serach.setVisibility(View.VISIBLE);
        serach.setText("扫一扫");
        serach.setOnClickListener(this);
        frontCheckLayout.setVisibility(View.GONE);
    }

    @Override
    public String setTitle() {
        return getString(R.string.setting_text);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.save_setting) {
            if (saveData()) {
                Intent intent = new Intent(this, HomepageActivity.class);
                startActivity(intent);
            }
        } else if (view.getId() == R.id.serach) {
            String[] perms = {Manifest.permission.CAMERA};
            if (EasyPermissions.hasPermissions(this, perms)) {//检查是否获取该权限
                Intent qrIntent = new Intent(this, MipcaActivityCapture.class);
                qrIntent.putExtra("type", 400);
                qrIntent.putExtra("title", "扫一扫");
                startActivityForResult(qrIntent, REQUEST_CODE);
            } else {
                int CAMERA_CODE = 0;
                EasyPermissions.requestPermissions(this, getString(R.string.get_permissions_tips), CAMERA_CODE, perms);
            }
        }
    }

    /**
     * 保存用户的配置数据
     */
    protected boolean saveData() {
        Map<String, String> map = new HashMap<>();
        String appidText = appid.getText().toString().trim();
        String appSecret = secretKey.getText().toString().trim();
        String number = terminalNumber.getText().toString().trim();
        String wokmenId = workmenId.getText().toString().trim();
        //选择语音提示1，否则为0；
        String playSpeak = speechCheck.isChecked() ? "1" : "0";
        if (appidText.equals("")) {
            ToastUtils.showToast(this, R.string.appid_input_tips);
            return false;
        }
        if (appSecret.equals("")) {
            ToastUtils.showToast(this, R.string.appid_input_tips);
            return false;
        }
        if (number.equals("")) {
            ToastUtils.showToast(this, R.string.terminal_number);
            return false;
        }
        map.put(Constants.APP_ID, appidText);
        map.put(Constants.APP_SECRET, appSecret);
        map.put(Constants.CODE, number);
        map.put(Constants.SPEAK_PLAY, playSpeak);
        map.put(Constants.WORK_MEN_ID, wokmenId);
        SharedPreferencesUtils.addMultipleToSharedPreferences(this, Constants.APP_INFO_SP_NAME, map);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            //处理扫描结果（在界面上显示）
            if (null != data) {
                Bundle bundle = data.getExtras();
                if (bundle == null) {
                    return;
                }
                String result = bundle.getString(CodeUtils.RESULT_STRING);
                String[] strs = new String[0];
                if (result != null) {
                    strs = result.split(";");
                }
                if (strs.length < 2) {
                    return;
                }
                appid.setText(strs[0]);
                secretKey.setText(strs[1]);
//
//                if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
//                    String result = bundle.getString(CodeUtils.RESULT_STRING);
//                    String[] strs = new String[0];
//                    if (result != null) {
//                        strs = result.split(";");
//                    }
//                    if (strs.length < 2) {
//                        return;
//                    }
//                    appid.setText(strs[0]);
//                    secretKey.setText(strs[1]);
//                } else if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_FAILED) {
//                    Toast.makeText(this, "解析二维码失败", Toast.LENGTH_LONG).show();
//                }
            }
        }
    }
}
