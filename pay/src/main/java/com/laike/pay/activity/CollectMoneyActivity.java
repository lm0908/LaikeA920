package com.laike.pay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.laike.pay.R;
import com.laike.pay.base.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CollectMoneyActivity extends BaseActivity {

    @Bind(R.id.brush_layout)
    LinearLayout brushLayout;//刷卡

    @Bind(R.id.sweep_layout)
    LinearLayout sweepLayout;//扫码

    @Bind(R.id.brushmg_layout)
    LinearLayout brushmgLayout;//刷卡管理

    @Bind(R.id.sweepmg_layout)
    LinearLayout sweepmgLayout;//扫码管理

    @Bind(R.id.brushmg_layout_all)
    LinearLayout brushmgAllLayout;//交易明细（包含刷卡和扫码）

    @Bind(R.id.now_sweep_layout)
    LinearLayout nowSweepLayout;//实时消费

    @Bind(R.id.settlement_layout)
    LinearLayout settlementlayout;//收款码

    public final static int INDEX_BRUSH = 1;//刷卡
    public final static int INDEX_SWEEEP = 2;//扫码
    public final static int INDEX_SWEEEP_NOW = 3;//实时消费

    @Override
    public int getContentViewResId() {
        return R.layout.activity_collect_money;
    }

    @Override
    public void intiViews() {
        brushLayout.setOnClickListener(this);
        sweepLayout.setOnClickListener(this);
        brushmgLayout.setOnClickListener(this);
        sweepmgLayout.setOnClickListener(this);
        brushmgAllLayout.setOnClickListener(this);
        nowSweepLayout.setOnClickListener(this);
        settlementlayout.setOnClickListener(this);
    }

    @Override
    public String setTitle() {
        return getString(R.string.collect_money);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        super.onAttachedToWindow();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Intent intent = new Intent(this, CollectMoneyActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        super.onClick(view);
        int id = view.getId();
        switch (id) {
            case R.id.brush_layout://刷卡
                intent = new Intent(this, BrushCard2Activity.class);
                intent.putExtra("type", -1);
                intent.putExtra("value", INDEX_BRUSH);
                break;
            case R.id.sweep_layout://扫码
                intent = new Intent(this, BrushCardActivity.class);
                intent.putExtra("value", INDEX_SWEEEP);
                break;
            case R.id.brushmg_layout:
                intent = new Intent(this, BrushCardManagerActivity.class);
                break;
            case R.id.sweepmg_layout:
                intent = new Intent(this, SweepManageActivity.class);
                break;
            case R.id.brushmg_layout_all:
                intent = new Intent(this, TransactionActivity.class);
                break;
            case R.id.settlement_layout:
                intent = new Intent(this, SweepGatheringActivity.class);
                break;
            case R.id.now_sweep_layout:
                intent = new Intent(this, BrushCard2Activity.class);
                intent.putExtra("type", -1);
                intent.putExtra("value", INDEX_SWEEEP_NOW);
                break;
        }
        if (null != intent) {
            startActivity(intent);
        }
    }
}
