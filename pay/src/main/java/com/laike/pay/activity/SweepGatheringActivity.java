package com.laike.pay.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.ImageView;

import com.laike.pay.R;
import com.laike.pay.base.BaseActivity;
import com.laike.pay.base.Constants;
import com.laike.pay.utils.SharedPreferencesUtils;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SweepGatheringActivity extends BaseActivity {
    @Bind(R.id.image)
    ImageView image;
    @Override
    public int getContentViewResId() {
        return R.layout.activity_sweep_gather;
    }

    @Override
    public void onAttachedToWindow() {
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        super.onAttachedToWindow();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Intent intent = new Intent(this, CollectMoneyActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void intiViews() {
        String url = SharedPreferencesUtils.getOneSharedPreferences(this, Constants.SHOP_INFO_SP_NAME, Constants.URL);
        Bitmap bitmap = CodeUtils.createImage(url, getResources().getDimensionPixelSize(R.dimen.erweima_height), getResources().getDimensionPixelSize(R.dimen.erweima_height), null);
        image.setImageBitmap(bitmap);
    }

    @Override
    public String setTitle() {
        return "扫码收款";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }
}
