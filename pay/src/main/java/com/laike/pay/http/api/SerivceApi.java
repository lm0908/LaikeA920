package com.laike.pay.http.api;

import com.laike.pay.bean.PayOrder;
import com.laike.pay.http.Result;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by wangshuo on 2017/5/17.
 */

public interface SerivceApi {
    /**
     * 收款下单
     */
    @FormUrlEncoded
    @POST("Api/Gateway.html")
    Call<Result<PayOrder>> payMent(@Field("total_amount") String total_amount,
                                   @Field("auth_code") String auth_code,
                                   @Field("appid") String appid,
                                   @Field("method") String method,
                                   @Field("code") String code,
                                   @Field("sign") String sign
    );


    /**
     * 扫码查单
     */
    @FormUrlEncoded
    @POST("Api/Gateway.html")
    Call<Result<PayOrder>> request(@Field("appid") String appid,
                                   @Field("method") String method,
                                   @Field("trade_no") String trade_no,
                                   @Field("out_trade_no") String out_trade_no,
                                   @Field("sign") String sign);


    /**
     * 签到
     */
    @FormUrlEncoded
    @POST("Api/Gateway.html")
    Call<Result<Map<String, Object>>> login(@Field("appid") String appid,
                                            @Field("method") String method,
                                            @Field("sign") String sign,
                                            @Field("terminal_id") String terminal_id,
                                            @Field("workmen_uid") String workmen_uid
    );

    /**
     * 退款
     */
    @FormUrlEncoded
    @POST("Api/Gateway.html")
    Call<Result<PayOrder>> refund(@Field("appid") String appid,
                                  @Field("method") String method,
                                  @Field("sign") String sign,
                                  @Field("trade_no") String trade_no,
                                  @Field("out_trade_no") String out_trade_no,
                                  @Field("workmen_uid") String workmen_uid
    );

    /**
     * 获取所有订单
     */
    @FormUrlEncoded
    @POST("Api/Gateway.html")
    Call<Result<Map<String, Object>>> getAllOrder(@Field("appid") String appid,
                                                  @Field("method") String method,
                                                  @Field("sign") String sign,
                                                  @Field("terminal_id") String terminal_id,
                                                  @Field("workmen_uid") String workmen_uid,
                                                  @Field("start_time") String start_time,
                                                  @Field("end_time") String end_time,
                                                  @Field("page") String page,
                                                  @Field("pagesize") String pagesize,
                                                  @Field("status") String status


    );

    /**
     * 下单
     */
    @FormUrlEncoded
    @POST("Api/Gateway.html")
    Call<Result<PayOrder>> getOrderId(@Field("appid") String appid,
                                      @Field("method") String method,
                                      @Field("code") String code,
                                      @Field("sign") String sign,
                                      @Field("total_amount") String total_amount,
                                      @Field("pay_type") String pay_type


    );

    /**
     * 下单
     */
    @GET
    Call<Result<Map<String, String>>> addOrder(@Url String url,
                                               @Query("out_trade_no") String out_trade_no,
                                               @Query("trade_no") String trade_no,
                                               @Query("pay_amount") String pay_amount,
                                               @Query("pay_type") String pay_type);


    /**
     * 获取所有订单
     */
    @FormUrlEncoded
    @POST("Api/Gateway.html")
    Call<Result<Map<String, Object>>> getBrushAllOrder(@Field("appid") String appid,
                                                       @Field("method") String method,
                                                       @Field("sign") String sign,
                                                       @Field("terminal_id") String terminal_id,
                                                       @Field("workmen_uid") String workmen_uid,
                                                       @Field("start_time") String start_time,
                                                       @Field("end_time") String end_time,
                                                       @Field("page") String page,
                                                       @Field("pagesize") String pagesize,
                                                       @Field("status") String status,
                                                       @Field("pay_type") String pay_type

    );

    /**
     * 更新版本
     */
    @FormUrlEncoded
    @POST("Api/Merapp.html")
    Call<Result<Map<String, String>>> updateVersion(
            @Field("method") String method,
            @Field("clienttype") String clienttype,
            @Field("sign") String sign);


}
