package com.laike.pay.http.api;

/**
 * Created by wangshuo on 2017/5/18.
 */

public class ApiName {
    /**
     * 收款
     */
    public static final String PAY_MENT = "payment.micropay";
    /**
     * 退款
     */
    public static final String RE_FUND = "payment.refundorder";
    /**
     * 扫码查单
     */
    public static final String SELECT_DEAL = "payment.orderquery";
    /**
     * login
     */
    public static final String LOGIN = "cashier.login";
    /**
     * 获取全部订单
     */
    public static final String GET_ALL_ORDER = "cashier.queryorderlist";
    /**
     * 下单
     */
    public static final String GET_ORDER_ID = "payment.unifiedorder";
    /**
     * 更新版本
     */
    public static final String UPDATE_VERSION = "User.User.app_version";
}
