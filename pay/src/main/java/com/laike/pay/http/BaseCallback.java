package com.laike.pay.http;

import retrofit2.Callback;

/**
 * Created by wangshuo on 2016/11/21.
 */

public interface BaseCallback<T> extends Callback<T> {

}
