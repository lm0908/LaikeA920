package com.laike.pay.http;

import java.util.Map;

/**
 * Created by wangshuo on 2016/11/21.
 * 网络请求返回数据结构
 */

public class Result<T> {

    private String status;
    private String info;
    private Map<String, String> form;
    private T data;

    public Map<String, Object> getEncrypted() {
        return encrypted;
    }

    public void setEncrypted(Map<String, Object> encrypted) {
        this.encrypted = encrypted;
    }

    private Map<String, Object> encrypted;

    public T getResult() {
        return data;
    }

    public void setResult(T result) {
        this.data = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Map<String, String> getForm() {
        return form;
    }

    public void setForm(Map<String, String> form) {
        this.form = form;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Result{" +
                "status='" + status + '\'' +
                ", info='" + info + '\'' +
                ", form=" + form +
                ", data=" + data +
                ", encrypted=" + encrypted +
                '}';
    }
}
