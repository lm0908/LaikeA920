package com.laike.pay.http;

import com.laike.pay.base.Constants;
import com.laike.pay.http.api.ApiName;
import com.laike.pay.utils.Md5Utils;
import com.laike.pay.utils.SharedPreferencesUtils;
import com.laike.pay.utils.SignUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by wangshuo on 2017/5/17.
 */

public class ParamsUtils {

    public static String getParams(String auth_code, String price) {
        Map<String, String> map = new HashMap<>();
        map.put("appid", SharedPreferencesUtils.getAppid());
        map.put("code", SharedPreferencesUtils.getTerminalNumber());
        map.put("auth_code", auth_code);
        map.put("total_amount", price);
        map.put("method", ApiName.PAY_MENT);
        String result = Md5Utils.addMd5(SignUtils.payParamsToString(map) + "&key=" + SharedPreferencesUtils.getAppSecret());
        return result;
    }

    /**
     * 扫码查单
     *
     * @return
     */
    public static String getOrderParams(String number) {
        Map<String, String> map = new HashMap<>();
        map.put("appid", SharedPreferencesUtils.getAppid());
        map.put("method", ApiName.SELECT_DEAL);
        if (number.length() == 17) {
            map.put("out_trade_no", number);
        } else if (number.length() > 17) {
            map.put("trade_no", number);
        }

        String result = Md5Utils.addMd5(SignUtils.payParamsToString(map) + "&key=" + SharedPreferencesUtils.getAppSecret());
        return result;
    }


    /**
     * 员工登陆
     *
     * @return
     */
    public static String login() {
        Map<String, String> map = new HashMap<>();
        map.put("appid", SharedPreferencesUtils.getAppid());
        map.put("method", ApiName.LOGIN);
        map.put("terminal_id", SharedPreferencesUtils.getTerminalNumber());
        map.put("workmen_uid", SharedPreferencesUtils.getWorkMenId());
        String result = Md5Utils.addMd5(SignUtils.payParamsToString(map) + "&key=" + SharedPreferencesUtils.getAppSecret());
        return result;
    }


    /**
     * @return
     */
    public static String refund(String number) {
        Map<String, String> map = new HashMap<>();
        map.put("appid", SharedPreferencesUtils.getAppid());
        map.put("method", ApiName.RE_FUND);
        if (number.length() == 17) {
            map.put("out_trade_no", number);
        } else if (number.length() > 17) {
            map.put("trade_no", number);
        }
        map.put("workmen_uid", SharedPreferencesUtils.getWorkMenId());
        String result = Md5Utils.addMd5(SignUtils.payParamsToString(map) + "&key=" + SharedPreferencesUtils.getAppSecret());
        return result;
    }


    /**
     * @return
     */
    public static String BrushRefund(String number) {
        Map<String, String> map = new HashMap<>();
        map.put("appid", SharedPreferencesUtils.getAppid());
        map.put("method", ApiName.RE_FUND);
        map.put("trade_no", number);
        map.put("workmen_uid", SharedPreferencesUtils.getWorkMenId());
        String result = Md5Utils.addMd5(SignUtils.payParamsToString(map) + "&key=" + SharedPreferencesUtils.getAppSecret());
        return result;
    }

    /**
     * 获取交易明细
     *
     * @return
     */
    public static String getAllOrder(String startTime, String endTime, String pageIndex, String pageNum, String status, String payType) {
        /**
         *  URL编码
         */
        if (startTime != null && !startTime.equals("")) {
            try {
                startTime = URLEncoder.encode(startTime, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        if (endTime != null && !endTime.equals("")) {
            try {
                endTime = URLEncoder.encode(endTime, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        Map<String, String> map = new HashMap<>();
        map.put("appid", SharedPreferencesUtils.getAppid());
        map.put("method", ApiName.GET_ALL_ORDER);
        map.put("terminal_id", SharedPreferencesUtils.getTerminalNumber());
        map.put("workmen_uid", SharedPreferencesUtils.getWorkMenId());
        map.put("start_time", startTime);
        map.put("end_time", endTime);
        map.put("page", pageIndex);
        map.put("pagesize", pageNum);
        map.put("status", status);
        if (!payType.equals("")) {
            map.put("pay_type", payType);
        }
        Set<String> set = map.keySet();
        String result = Md5Utils.addMd5(SignUtils.payParamsToString(map) + "&key=" + SharedPreferencesUtils.getAppSecret());
        return result;
    }

    public static String getOrderId(String total_amount, String pay_type) {
        Map<String, String> map = new HashMap<>();
        map.put("appid", SharedPreferencesUtils.getAppid());
        map.put("code", SharedPreferencesUtils.getTerminalNumber());
        map.put("method", ApiName.GET_ORDER_ID);
        map.put("total_amount", total_amount);
        map.put("pay_type", pay_type);
        String result = Md5Utils.addMd5(SignUtils.payParamsToString(map) + "&key=" + SharedPreferencesUtils.getAppSecret());
        return result;
    }
    public static String getUpdateVersion() {
        Map<String, String> map = new HashMap<>();
        map.put("method", ApiName.UPDATE_VERSION);
        map.put("clienttype", Constants.NEW_VERSION_NAME);
        String result = Md5Utils.addMd5(SignUtils.payParamsToString(map) + "&key=" + Constants.NEW_VERSION_SECRET);
        return result;
    }

}
