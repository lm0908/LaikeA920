package com.laike.pay.http;


import android.support.annotation.NonNull;
import android.util.Log;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by wangshuo on 2016/11/21.
 */

public class HttpRetrofit {
    private static final String TAG = "HttpRetrofit";
    private static Converter.Factory gsonConverterFactory = GsonConverterFactory.create();
    //    private static final String BASE_URL = "http://192.168.199.107:8090/";//后台本地服务器
    private static final String BASE_URL = "http://www.laike666.com/";//阿里服务器
    private static Retrofit retrofit;
    private final static HttpRetrofit httpRetrofit = new HttpRetrofit();
    private static final int DEFAULT_TIMEOUT = 60;
    //是否需要md5加密，默认true。根据接口具体情况使用


    private HttpRetrofit() {

    }

    public static HttpRetrofit getInstance() {
        if (null == retrofit) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(gsonConverterFactory)
                    .client(genericClient())
                    .build();
        }
        return httpRetrofit;
    }
    public static void loadFile(String url, @NonNull final ProgressListener listener) {
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                        .body(new ProgressResponseBody(originalResponse.body(), new ProgressListener() {
                            @Override
                            public void onProgress(long progress, long total, boolean done) {
                                listener.onProgress(progress, total, done);
                            }
                        }))
                        .build();
            }
        };
        OkHttpClient client = genericClient().newBuilder().addNetworkInterceptor(interceptor).build();
        Request.Builder builder = new Request.Builder();
        builder.get().url(url);
        Request request = builder.build();
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onException(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ResponseBody body = response.body();
                byte[] bytes = body.bytes();
                listener.onHttpSuccess(bytes);
            }
        });
    }



    /**
     * @param obj
     * @param <T>
     * @return
     */
    public <T> T getInterfaceApi(Class<T> obj) {

        return retrofit.create(obj);
    }

    public static OkHttpClient genericClient() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request requestUrl = chain.request();
                        //添加请求头部
                        Request request = requestUrl.newBuilder()
                                //添加http头部使用
//                                .addHeader("appid", curTimes)
                                .method(requestUrl.method(), requestUrl.body())
//                                .url(setParameterSign(requestUrl))
                                .build();
                        Log.v("httpURL", "httpURL-----" + request.url());

                        return chain.proceed(request);
                    }

                })
                .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .build();


        return httpClient;
    }
//
//    /**
//     * 添加session，token
//     *
//     * @param requestUrl
//     * @return
//     */
//    private static HttpUrl setParameter(Request requestUrl) {
//        HttpUrl url = requestUrl.url();
//        return url.newBuilder()
//                .scheme(requestUrl.url().scheme())
//                .host(requestUrl.url().host())
//                .addQueryParameter("appid", "28dd2c7955ce926456240b2ff0100bde")
//                .addQueryParameter("code", "810253")
//                .build();
//    }

//    private static HttpUrl setParameterSign(Request requestUrl) {
//        HttpUrl url = setParameter(requestUrl);
//        Map<String, String> map = new HashMap<String, String>();
//        Set<String> set = url.queryParameterNames();
//        for (String key : set) {
//            Log.v("key", "-----" + key);
//            Log.v("value", "-----" + url.queryParameter(key));
//            map.put(key, url.queryParameter(key));
//        }
//        String sign = Md5Utils.addMd5(SignUtils.payParamsToString(map) + "&key=" + "FxBla6kzMmW4dIZLt13jireEyg9VDN7w");
//        Log.v("Parameter", "-----" + url.query());
//        Log.v("sign", "---sign--" + sign);
//        return url.newBuilder()
//                .scheme(requestUrl.url().scheme())
//                .host(requestUrl.url().host())
//                .addQueryParameter("sign", sign)
//                .build();
//
//    }


}
