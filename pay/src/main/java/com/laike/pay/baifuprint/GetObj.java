package com.laike.pay.baifuprint;

import android.util.Log;

import com.laike.pay.activity.HomepageActivity;
import com.laike.pay.base.BaseActivity;
import com.pax.gl.IGL;
import com.pax.ippi.dal.interfaces.IDal;
import com.pax.ippi.emv.interfaces.IEmv;

/**
 * Created by xlm on 2018/3/13 0013
 */

public class GetObj {
    private static IDal dal;
    private static IGL gl;
    private static IEmv emv;

    // 获取IDal dal对象
    public static IDal getDal() {
        dal = BaseActivity.dal;
        if (dal == null) {
            Log.e("IPPITest", "dal is null");
        }
        return dal;
    }

    // 获取IGL gl对象
    public static IGL getGL() {

        gl = BaseActivity.gl;
        if (gl == null) {
            Log.e("IPPITest", "gl is null");
        }
        return gl;

    }

    // 获取IEmv emv对象
    public static IEmv getEmv() {

        emv = BaseActivity.emv;
        if (emv == null) {
            Log.e("IPPITest", "emv is null");
        }
        return emv;

    }
}
