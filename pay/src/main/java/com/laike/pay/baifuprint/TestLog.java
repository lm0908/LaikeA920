package com.laike.pay.baifuprint;

import android.util.Log;

/**
 * Created by xlm on 2018/3/13 0013
 */

public class TestLog {
    private String childName = "";

    public TestLog() {
        childName = getClass().getSimpleName() + ".";
    }

    public void logTrue(String method) {
        Log.i("IPPITest", childName + method);
    }

    public void logErr(String method, String errString) {
        Log.e("IPPITest", childName + method + "   出错信息：" + errString);
    }
}
