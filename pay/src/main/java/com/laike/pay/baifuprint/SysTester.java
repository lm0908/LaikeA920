package com.laike.pay.baifuprint;
import com.laike.pay.activity.HomepageActivity;
import com.pax.ipp.service.aidl.Exceptions;
import com.pax.ipp.service.aidl.dal.sys.EBeepMode;
import com.pax.ipp.service.aidl.dal.sys.ENavigationKey;
import com.pax.ipp.service.aidl.dal.sys.ETermInfoKey;
import com.pax.ipp.service.aidl.dal.sys.ETouchMode;
import com.pax.ippi.dal.interfaces.ISys;

import java.util.Map;
/**
 * Created by xlm on 2018/3/13 0013
 */
public class SysTester extends TestLog {

    private static SysTester sysTester;
    private ISys iSys = null;

    private SysTester() {
        iSys = GetObj.getDal().getSys();
    }

    public static SysTester getInstance() {
        if (sysTester == null) {
            sysTester = new SysTester();
        }
        return sysTester;
    }

    public void beep(final EBeepMode beepMode, final int delayTime) {
        try {
            iSys.beep(beepMode, delayTime);
            logTrue("beep");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("beep", e.toString());
        }

    }

    public String getTerminfo() {
        try {
            Map<ETermInfoKey, String> termInfo = iSys.getTermInfo();
            logTrue("getTerminfo");
            StringBuilder termInfoStr = new StringBuilder();
            for (ETermInfoKey key : ETermInfoKey.values()) {
                termInfoStr.append(key.name() + ":" + termInfo.get(key) + "\n");
            }
            return termInfoStr.toString();
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("getTerminfo", e.toString());
            return "";
        }

    }

    public String getTerminfoModel() {
        try {
            Map<ETermInfoKey, String> termInfo = iSys.getTermInfo();
            logTrue("getTerminfoModel");
            return termInfo.get(ETermInfoKey.MODEL);
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("getTerminfoModel", e.toString());
            return "";
        }
    }

    public String getRadom(int len) {
        try {
            byte[] random = iSys.getRandom(len);
            if (random != null) {
                logTrue("getRadom");
                return HomepageActivity.gl.getConvert().bcdToStr(random);
            } else {
                logErr("getRadom", "return null");
                return "null";
            }
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("getRadom", e.toString());
            return "";
        }

    }

    public String getDevInterfaceVer() {
        try {
            String verString = iSys.getDevInterfaceVer();
            logTrue("getDevInterfaceVer");
//            return "version of device interface:" + verString;
            return verString;
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("getDevInterfaceVer", e.toString());
            return "";
        }
    }

    public void showNavigationBar(boolean flag) {
        try {
            iSys.showNavigationBar(flag);
            logTrue("showNavigationBar");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("showNavigationBar", e.toString());
        }
    }

    public void enableNavigationBar(boolean flag) {
        try {
            iSys.enableNavigationBar(flag);
            logTrue("enableNavigationBar");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("enableNavigationBar", e.toString());
        }
    }

    public void enableNavigationKey(ENavigationKey navigationKey, boolean flag) {
        try {
            iSys.enableNavigationKey(navigationKey, flag);
            logTrue("enableNavigationKey");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("enableNavigationKey", e.toString());
        }
    }

    public boolean isNavigationBarVisible() {
        boolean res = true;
        try {
            res = iSys.isNavigationBarVisible();
            logTrue("isNavigationBarVisible");
            return res;
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("isNavigationBarVisible", e.toString());
        }
        return res;
    }

    public boolean isNavigationBarEnabled() {
        boolean res = true;
        try {
            res = iSys.isNavigationBarEnabled();
            logTrue("isNavigationBarEnabled");
            return res;
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("isNavigationBarEnabled", e.toString());
        }
        return res;
    }

    public boolean isNavigationKeyEnabled(ENavigationKey navigationKey) {
        boolean res = true;
        try {
            res = iSys.isNavigationKeyEnabled(navigationKey);
            logTrue("isNavigationKeyEnabled");
            return res;
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("isNavigationKeyEnabled", e.toString());
        }
        return res;
    }

    public void showStatusBar(boolean flag) {
        try {
            iSys.showStatusBar(flag);
            logTrue("showStatusBar");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("showStatusBar", e.toString());
        }
    }

    public void enableStatusBar(boolean flag) {
        try {
            iSys.enableStatusBar(flag);
            logTrue("enableStatusBar");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("enableStatusBar", e.toString());
        }
    }

    public boolean isStatusBarEnabled() {
        boolean res = true;
        try {
            res = iSys.isStatusBarEnabled();
            logTrue("isStatusBarEnabled");
            return res;
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("isStatusBarEnabled", e.toString());
        }
        return res;
    }

    public boolean isStatusBarVisible() {
        boolean res = true;
        try {
            res = iSys.isStatusBarVisible();
            logTrue("isStatusBarVisible");
            return res;
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("isStatusBarVisible", e.toString());
        }
        return res;
    }

    public void resetStatusBar() {
        try {
            iSys.resetStatusBar();
            logTrue("resetStatusBar");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("resetStatusBar", e.toString());
        }
    }

    public void enablePowerKey(boolean flag) {
        try {
            iSys.enablePowerKey(flag);
            logTrue("enablePowerKey");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("enablePowerKey", e.toString());
        }
    }

    public boolean isPowerKeyEnabled() {
        boolean res = true;
        try {
            res = iSys.isPowerKeyEnabled();
            logTrue("isPowerKeyEnabled");
            return res;
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("isPowerKeyEnabled", e.toString());
        }
        return res;
    }

    public void setSettingsNeedPassword(boolean flag) {
        try {
            iSys.setSettingsNeedPassword(flag);
            logTrue("setSettingsNeedPassword");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("setSettingsNeedPassword", e.toString());
        }
    }

    public void reboot() {
        try {
            iSys.reboot();
            logTrue("reboot");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("reboot", e.toString());
        }
    }

    public void shutdown() {
        try {
            iSys.shutdown();
            logTrue("shutdown");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("shutdown", e.toString());
        }
    }

    public void switchTouchMode(ETouchMode touchMode) {
        try {
            iSys.switchTouchMode(touchMode);
            logTrue("switchTouchMode");
        } catch (Exceptions e) {
            e.printStackTrace();
            logErr("switchTouchMode", e.toString());
        }
    }

}
