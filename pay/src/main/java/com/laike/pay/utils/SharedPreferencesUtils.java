package com.laike.pay.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.laike.pay.base.Constants;
import com.laike.pay.base.MyApplication;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by wangshuo on 2016/11/4.
 */

public class SharedPreferencesUtils {

    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static SharedPreferencesUtils spu = null;

    private SharedPreferencesUtils() {

    }

    public static SharedPreferencesUtils getIntance() {
        if (spu == null) {
            spu = new SharedPreferencesUtils();
        }
        return spu;
    }


    /**
     * 一次存一个
     */
    public static void addOneToSharedPreferences(Context context, String spName, String key, String value) {
        SharedPreferences mySharedPreferences = context.getSharedPreferences(spName, Activity.MODE_PRIVATE);
        //实例化SharedPreferences.Editor对象（第二步）
        SharedPreferences.Editor editor = mySharedPreferences.edit();
        //用putString的方法保存数据
        editor.putString(key, value);
        //提交当前数据
        editor.commit();
    }

    /**
     * 一次存多个
     */
    public static boolean addMultipleToSharedPreferences(Context context, String spName, Map<String, String> map) {
        SharedPreferences mySharedPreferences = context.getSharedPreferences(spName, Activity.MODE_PRIVATE);
        //实例化SharedPreferences.Editor对象（第二步）
        SharedPreferences.Editor editor = mySharedPreferences.edit();
        //用putString的方法保存数据
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            editor.putString(entry.getKey(), entry.getValue());
//            System.out.println("entry.getKey()====" + entry.getKey() + "--" + entry.getValue());
        }
        //提交当前数据
        return editor.commit();
    }


    /**
     * 读取一个数据
     */
    public static String getOneSharedPreferences(Context context, String spName, String key) {

        String values = null;
        SharedPreferences mySharedPreferences = context.getSharedPreferences(spName, Activity.MODE_PRIVATE);
        //如果没有该key。默认返回""
        values = mySharedPreferences.getString(key, "");
        return values;
    }

    /**
     * 读取多个数据
     */
    public static List<String> getMultipleSharedPreferences(Context context, String spName, List<String> list) {
        List<String> values = new ArrayList<String>();
        SharedPreferences mySharedPreferences = context.getSharedPreferences(spName, Activity.MODE_PRIVATE);

        for (int i = 0; i < list.size(); i++) {
            values.add(mySharedPreferences.getString(list.get(i), ""));
        }
        return values;
    }

    /**
     * 获取appid
     *
     * @return
     */
    public static String getAppid() {
        return getOneSharedPreferences(MyApplication.getContext(), Constants.APP_INFO_SP_NAME, Constants.APP_ID);
    }

    /**
     * 获取密钥
     *
     * @return
     */
    public static String getAppSecret() {
        return getOneSharedPreferences(MyApplication.getContext(), Constants.APP_INFO_SP_NAME, Constants.APP_SECRET);
    }

    /**
     * 获取终端号
     *
     * @return
     */
    public static String getTerminalNumber() {
        return getOneSharedPreferences(MyApplication.getContext(), Constants.APP_INFO_SP_NAME, Constants.CODE);
    }

    /**
     * 获取palyspeak
     *
     * @return
     */
    public static String getPlaySpeak() {
        return getOneSharedPreferences(MyApplication.getContext(), Constants.APP_INFO_SP_NAME, Constants.SPEAK_PLAY);
    }

    /**
     * 获取palyspeak
     *
     * @return
     */
    public static String getFrontChecked() {
        return getOneSharedPreferences(MyApplication.getContext(), Constants.APP_INFO_SP_NAME, Constants.IS_FRONT_CAMERA);
    }

    /**
     * 获取员工id
     *
     * @return
     */
    public static String getWorkMenId() {
        return getOneSharedPreferences(MyApplication.getContext(), Constants.APP_INFO_SP_NAME, Constants.WORK_MEN_ID);
    }


    public static boolean deleteAll(Context context, String spName) {
        SharedPreferences mySharedPreferences = context.getSharedPreferences(spName, Activity.MODE_PRIVATE);
        if (mySharedPreferences != null) {
            return mySharedPreferences.edit().clear().commit();
        }
        return false;
    }
}
