package com.laike.pay.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.laike.pay.activity.DLB_MainActivity;
import com.orhanobut.logger.Logger;

import java.util.List;

/**
 * Created by Administrator on 2018\4\27 0027.
 */public class  CommonUtil{
    /**
     * 判断某个Activity 界面是否在前台
     * @param context
     * @return
     */
    public static void isForeground(Context context) {
        if (context == null || TextUtils.isEmpty("DLB_MainActivity")) {
            return;
        }
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = null;
        if (am != null) {
            list = am.getRunningTasks(1);
        }
        if (list != null && list.size() > 0) {
            ComponentName cpn = list.get(0).topActivity;
            if ("DLB_MainActivity".equals(cpn.getClassName())) {
            }else {
                Logger.d("------getClassName------"+cpn.getClassName());
                Intent i = new Intent(context, DLB_MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        }
    }
}