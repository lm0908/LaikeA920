package com.laike.pay.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wangshuo on 2017/5/19.
 */

public class TimeUtils {
    /*
   * 将时间戳转换为时间
   */
    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

    /*
  * 将时间戳转换为时间
  */
    public static String stampToDate(String s, String format) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

    /**
     * string to date
     *
     * @param s
     * @param format
     * @return
     */
    public static Date strToDate(String s, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        long lt = new Long(s);
        Date date = new Date(lt);
        return new Date(lt);
    }
}
