package com.laike.pay.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.laike.pay.R;

/**
 * Created by wangshuo on 2017/5/17.
 */

public class ProgressDialogUtils {
    static AlertDialog mDialog = null;


    public static AlertDialog getmDialog() {
        return mDialog;
    }

    /**
     * 显示dialog
     *
     * @param context
     */
    public static void showProgress(Activity context) {
        showProgress(context, null, null);
    }

    /**
     * 显示默认加载dialog
     *
     * @param context
     */
    public static void showProgressLoading(Activity context) {
        showProgress(context, null, context.getString(R.string.common_loading));
    }

    /**
     * 显示dialog
     *
     * @param context
     * @param messageId 内容id
     */
    public static void showProgress(Activity context, int messageId) {
        showProgress(context, 0, messageId);
    }

    /**
     * 显示dialog
     *
     * @param context
     * @param titleId   标题id
     * @param messageId 内容id
     */
    public static void showProgress(Activity context, int titleId, int messageId) {
        showProgress(context, titleId > 0 ? context.getString(titleId) : null,
                messageId > 0 ? context.getString(messageId) : null);
    }

    /**
     * @param context
     * @param message 内容
     */
    public static void showProgress(Activity context, String message) {

        showProgress(context, null, message);
    }

    /**
     * 显示dialog
     *
     * @param context
     * @param title   标题
     * @param message 内容
     */
    public static void showProgress(Activity context, String title, String message) {
        dismissProgress();

        View view = LayoutInflater.from(context).inflate(R.layout.dlg_progress, null);
        TextView msgVIew = (TextView) view.findViewById(R.id.tv_progress_msg);

        msgVIew.setText(message);
//	DialogUtils.showDialog(context, view);

        mDialog = new AlertDialog.Builder(context).create();
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable());
        mDialog.setCancelable(false);
        mDialog.show();

        mDialog.setContentView(view);

        Window win = mDialog.getWindow();

        WindowManager.LayoutParams params = win.getAttributes();

        params.height = context.getResources().getDimensionPixelOffset(R.dimen.common_progressdlg_size);
        params.width = context.getResources().getDimensionPixelOffset(R.dimen.common_progressdlg_size_x);

        win.setAttributes(params);
    }

    /**
     * 隐藏dialog
     */
    public static void dismissProgress() {
        if (null != mDialog) {
            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }
            mDialog = null;
        }
    }
}
