package com.laike.pay.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.laike.pay.activity.DLB_MainActivity;

/**
 * Created by xlm on 2018/3/2 0002
 */

public class AutoPowerReceiver extends BroadcastReceiver {
    public AutoPowerReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Intent i = new Intent(context, DLB_MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
}
