package com.laike.pay.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by xlm on 2018/3/29 0029
 */

public class VersionUtil {
    /**
     * 版本名
     *
     * @param context
     * @return
     */
    public static String getVersionName(Context context) {
        return getPackageInfo(context).versionName;
    }

    /**
     * 版本号
     *
     * @param context
     * @return
     */
    public static int getVersionCode(Context context) {
        return getPackageInfo(context).versionCode;
    }

    /**
     * @param version1
     * @param version2
     * @return 0 相等， 1 前者大,  2后者大
     */
    public static int compareVersionName(String version1, String version2) {
        int result = 0;
        String[] v1 = version1.split("\\.");
        String[] v2 = version2.split("\\.");
        int length1 = v1.length;
        int length2 = v2.length;
        if (length1 == length2) {
            for (int i = 0; i < length1; i++) {
                int v1i = Integer.valueOf(v1[i]);
                int v2i = Integer.valueOf(v2[i]);
                if (v1i > v2i) {
                    result = 1;
                    break;
                } else if (v1i == v2i) {
                    result = 0;
                } else {
                    result = 2;
                    break;
                }
            }
        } else if (length1 < length2) {
            for (int i = 0; i < length1; i++) {
                int v1i = Integer.valueOf(v1[i]);
                int v2i = Integer.valueOf(v2[i]);
                if (v1i > v2i) {
                    result = 1;
                    break;
                } else if (v1i == v2i) {
                    result = 2;
                } else {
                    result = 2;
                    break;
                }
            }
        } else {
            for (int i = 0; i < length2; i++) {
                int v1i = Integer.valueOf(v1[i]);
                int v2i = Integer.valueOf(v2[i]);
                if (v1i > v2i) {
                    result = 1;
                    break;
                } else if (v1i == v2i) {
                    result = 1;
                } else {
                    result = 2;
                    break;
                }
            }
        }
        return result;
    }


    private static PackageInfo getPackageInfo(Context context) {
        PackageInfo pi = null;

        try {
            PackageManager pm = context.getPackageManager();
            pi = pm.getPackageInfo(context.getPackageName(),
                    PackageManager.GET_CONFIGURATIONS);

            return pi;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pi;
    }

    public static boolean deleteFile(File file) {
        if (!file.exists()) {
            return true;
        } else if (file.isDirectory()) {
            File[] files = file.listFiles();
            boolean result = true;
            for (File child : files) {
                if (!deleteFile(child)) {
                    result = false;
                    break;
                }
            }
            return result;
        } else {
            return file.delete();
        }
    }

    public static boolean deleteFileSafely(File file) {
        if (file != null) {
            String tmpPath = file.getParent() + File.separator + System.currentTimeMillis();
            File tmp = new File(tmpPath);
            file.renameTo(tmp);
            return tmp.delete();
        }
        return false;
    }

    public static void byte2File(byte[] buf, String filePath, String fileName) throws IOException {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        File file = null;
        File dir = new File(filePath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        file = new File(filePath + File.separator + fileName);
        fos = new FileOutputStream(file);
        bos = new BufferedOutputStream(fos);
        bos.write(buf);
        if (bos != null) {
            bos.close();
        }
        if (fos != null) {
            fos.close();
        }
    }

    public static void installApk(Context context, String path) {
//        context 必须是activity
        Intent intent = new Intent(Intent.ACTION_VIEW);
        // 判断版本大于等于7.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            setPermission(path);
            File file = (new File(path));
            //参数1 上下文, 参数2 Provider主机地址 和配置文件中保持一致   参数3  共享的文件
            Uri apkUri = FileProvider.getUriForFile(context, "com.duolaibei.pay.FileProvider", file);
            //添加这一句表示对目标应用临时授权该Uri所代表的文件
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/vnd.android.package-archive");
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private static void setPermission(String path) {
        String command = "chmod " + "777" + " " + path;
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
