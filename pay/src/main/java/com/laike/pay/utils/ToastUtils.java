package com.laike.pay.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by wangshuo on 2017/5/14.
 */

public class ToastUtils {
    static Toast mToast;

    /**
     * 显示toast
     *
     * @param context
     * @param resId
     */
    public static void showToast(Context context, int resId) {
        if (null != context)
            showToast(context, context.getResources().getString(resId));
    }

    /**
     * 显示toast
     *
     * @param context
     * @param text
     */
    public static void showToast(Context context, String text) {

        if (null != mToast)
            mToast.cancel();

        if (null != context) {
            mToast = Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_LONG);
            mToast.show();
        }
    }

    /**
     * 显示toast
     *
     * @param context
     * @param text
     */
    public static void showToast(Context context, String text, int duration) {

        if (null != mToast)
            mToast.cancel();

        if (null != context) {
            mToast = Toast.makeText(context.getApplicationContext(), text, duration);
            mToast.show();
        }
    }
}
