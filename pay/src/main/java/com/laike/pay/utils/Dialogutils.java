package com.laike.pay.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;

import com.laike.pay.R;


/**
 * Created by wangshuo on 2016/10/26.
 */

public class Dialogutils {
    /**
     * 消失监听
     *
     * @author Hanbing
     * @date 2015-7-27
     */
    public interface OnDismissListener {
        /**
         * 点击确定
         */
        public void onConfirm(String ContentTxet);

        /**
         * 点击取消
         */
        public void onCancel();
    }

    static Dialog mDialog;

    /**
     * 内容视图
     */
    static View mContentView;

    /**
     * 标题
     */
    // static TextView mTitle;
    /**
     * 消息
     */
    static TextView mMessage;
    /**
     * 确定按钮
     */
    static TextView mConfirm;
    /**
     * 取消按钮
     */
    static TextView mCancel;

    public static Dialog getDialog() {
        if (null != mDialog) {
            return mDialog;
        } else {
            return null;
        }

    }

    /**
     * 从parent移除
     *
     * @param contentView
     * @return
     */
    static View removeFromParent(View contentView) {
        if (null != contentView) {
            ViewParent parent = contentView.getParent();
            if (null != parent) {
                if (parent instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) parent;
                    viewGroup.removeView(contentView);
                } else {
                    contentView = null;
                }
            }
        }

        return contentView;
    }

    /**
     * 初始化内容
     *
     * @param context
     */
    static void initContentView(Context context) {
        mContentView = removeFromParent(mContentView);

        if (null == mContentView) {
            mContentView = LayoutInflater.from(context).inflate(R.layout.common_dialog_layout, null);

            mMessage = (TextView) mContentView.findViewById(R.id.dialog_content);
            mConfirm = (TextView) mContentView.findViewById(R.id.btn_confirm);
            mCancel = (TextView) mContentView.findViewById(R.id.btn_cancel);
        }

    }

    /**
     * 显示提示框
     *
     * @param context
     * @param contentView 内容界面
     */
    public static void showDialog(Context context, View contentView) {
        dismissDialog();

        contentView = removeFromParent(contentView);

        if (null != contentView) {
            mDialog = new Dialog(context, R.style.CommonDialogStyle);
            mDialog.setContentView(contentView);
            mDialog.show();
        }

    }

    public static void showDialog(Context context, String message,
                                  final OnDismissListener lsner) {
        showDialog(context, null, message, lsner);
    }

    /**
     * 显示提示框
     *
     * @param context
     * @param title
     * @param message
     * @param lsner
     */
    private static void showDialog(Context context, String title, final String message, final OnDismissListener lsner) {

        dismissDialog();

        initContentView(context);

        mDialog = new Dialog(context, R.style.CommonDialogStyle);
        mDialog.setContentView(mContentView);

        mMessage.setText(message);
        mConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDialog.dismiss();
                if (null != lsner) {
                    String contentTxet = mMessage.getText().toString().trim();
                    lsner.onConfirm(contentTxet);
                }
            }
        });
        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDialog.dismiss();
                if (null != lsner) {
                    lsner.onCancel();
                }
            }
        });

        mDialog.show();


    }

    /**
     * 隐藏提示框
     */
    public static void dismissDialog() {
        if (null != mDialog && mDialog.isShowing()) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
