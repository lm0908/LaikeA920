package com.laike.pay.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by wangshuo on 2017/5/26.
 */

public class DateUtils {
    private final static String FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * string转date
     *
     * @return
     * @throws ParseException
     */
    public static boolean stringToDate(String startTime, String endTime) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
        Date startDate = sdf.parse(startTime);
        SimpleDateFormat sdf1 = new SimpleDateFormat(FORMAT);
        Date endDate = sdf1.parse(endTime);
        return startDate.before(endDate);

    }

    /**
     * 得到两个日期相差的天数
     */
    public static int getBetweenDay(Date date1, Date date2) {
        Calendar d1 = new GregorianCalendar();
        d1.setTime(date1);
        Calendar d2 = new GregorianCalendar();
        d2.setTime(date2);
        int days = d2.get(Calendar.DAY_OF_YEAR) - d1.get(Calendar.DAY_OF_YEAR);
        System.out.println("days=" + days);
        int y2 = d2.get(Calendar.YEAR);
        if (d1.get(Calendar.YEAR) != y2) {
//          d1 = (Calendar) d1.clone();
            do {
                days += d1.getActualMaximum(Calendar.DAY_OF_YEAR);
                d1.add(Calendar.YEAR, 1);
            } while (d1.get(Calendar.YEAR) != y2);
        }
        return days;
    }
}
