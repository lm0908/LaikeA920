package com.laike.pay.bean;


public class Detail {


    private String paystyle;
    private String money;
    private String paytime;
    private String payresult;

    public Detail(String paystyle, String money, String paytime, String payresult) {
        this.paystyle = paystyle;
        this.money = money;
        this.paytime = paytime;
        this.payresult = payresult;

    }

    public Detail() {


    }

    public String getPaystyle() {
        return paystyle;
    }

    public void setPaystyle(String paystyle) {
        this.paystyle = paystyle;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getPaytime() {
        return paytime;
    }

    public void setPaytime(String paytime) {
        this.paytime = paytime;
    }

    public String getPayresult() {
        return payresult;
    }

    public void setPayresult(String payresult) {
        this.payresult = payresult;
    }

    @Override
    public String toString() {
        return "Detail{" +
                "paystyle='" + paystyle + '\'' +
                ", money='" + money + '\'' +
                ", paytime='" + paytime + '\'' +
                ", payresult='" + payresult + '\'' +
                '}';
    }
}
