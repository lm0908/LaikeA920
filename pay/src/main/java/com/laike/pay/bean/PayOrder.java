package com.laike.pay.bean;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by wangshuo on 2017/5/17.
 */
@Entity
public class PayOrder implements Parcelable {
    @Id(autoincrement = true)
    private Long id;
    private String out_trade_no;
    private String notify_count;
    private String notify_url;
    private String rqcode_url;
    private String body;
    private String uid;
    private String trade_no;
    private String service;
    private String product_id;
    private String pay_type;
    private String pay_name;
    private String total_amount;
    private String discountable_amount;
    private String pay_amount;
    private String pay_uid;
    private String shop_id;
    private String terminal_id;
    private String agent_uid;
    private String status;
    private String paytime;
    private String addtime;
    private String uptime;
    private String external_order_no;
    private String workmen_id;

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    @Generated(hash = 1826670845)
    public PayOrder(Long id, String out_trade_no, String notify_count, String notify_url,
                    String rqcode_url, String body, String uid, String trade_no, String service,
                    String product_id, String pay_type, String pay_name, String total_amount,
                    String discountable_amount, String pay_amount, String pay_uid, String shop_id,
                    String terminal_id, String agent_uid, String status, String paytime, String addtime,
                    String uptime, String external_order_no, String workmen_id) {
        this.id = id;
        this.out_trade_no = out_trade_no;
        this.notify_count = notify_count;
        this.notify_url = notify_url;
        this.rqcode_url = rqcode_url;
        this.body = body;
        this.uid = uid;
        this.trade_no = trade_no;
        this.service = service;
        this.product_id = product_id;
        this.pay_type = pay_type;
        this.pay_name = pay_name;
        this.total_amount = total_amount;
        this.discountable_amount = discountable_amount;
        this.pay_amount = pay_amount;
        this.pay_uid = pay_uid;
        this.shop_id = shop_id;
        this.terminal_id = terminal_id;
        this.agent_uid = agent_uid;
        this.status = status;
        this.paytime = paytime;
        this.addtime = addtime;
        this.uptime = uptime;
        this.external_order_no = external_order_no;
        this.workmen_id = workmen_id;
    }

    @Generated(hash = 797263523)
    public PayOrder() {
    }

    protected PayOrder(Parcel in) {
        out_trade_no = in.readString();
        notify_count = in.readString();
        body = in.readString();
        uid = in.readString();
        trade_no = in.readString();
        service = in.readString();
        product_id = in.readString();
        pay_type = in.readString();
        pay_name = in.readString();
        total_amount = in.readString();
        discountable_amount = in.readString();
        pay_amount = in.readString();
        pay_uid = in.readString();
        shop_id = in.readString();
        terminal_id = in.readString();
        agent_uid = in.readString();
        status = in.readString();
        paytime = in.readString();
        addtime = in.readString();
        uptime = in.readString();
        external_order_no = in.readString();
        workmen_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(out_trade_no);
        dest.writeString(notify_count);
        dest.writeString(body);
        dest.writeString(uid);
        dest.writeString(trade_no);
        dest.writeString(service);
        dest.writeString(product_id);
        dest.writeString(pay_type);
        dest.writeString(pay_name);
        dest.writeString(total_amount);
        dest.writeString(discountable_amount);
        dest.writeString(pay_amount);
        dest.writeString(pay_uid);
        dest.writeString(shop_id);
        dest.writeString(terminal_id);
        dest.writeString(agent_uid);
        dest.writeString(status);
        dest.writeString(paytime);
        dest.writeString(addtime);
        dest.writeString(uptime);
        dest.writeString(external_order_no);
        dest.writeString(workmen_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PayOrder> CREATOR = new Creator<PayOrder>() {
        @Override
        public PayOrder createFromParcel(Parcel in) {
            return new PayOrder(in);
        }

        @Override
        public PayOrder[] newArray(int size) {
            return new PayOrder[size];
        }
    };

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOut_trade_no() {
        return this.out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getNotify_count() {
        return this.notify_count;
    }

    public void setNotify_count(String notify_count) {
        this.notify_count = notify_count;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTrade_no() {
        return this.trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }

    public String getService() {
        return this.service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getPay_type() {
        return this.pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getPay_name() {
        return this.pay_name;
    }

    public void setPay_name(String pay_name) {
        this.pay_name = pay_name;
    }

    public String getTotal_amount() {
        return this.total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getDiscountable_amount() {
        return this.discountable_amount;
    }

    public void setDiscountable_amount(String discountable_amount) {
        this.discountable_amount = discountable_amount;
    }

    public String getPay_amount() {
        return this.pay_amount;
    }

    public void setPay_amount(String pay_amount) {
        this.pay_amount = pay_amount;
    }

    public String getPay_uid() {
        return this.pay_uid;
    }

    public void setPay_uid(String pay_uid) {
        this.pay_uid = pay_uid;
    }

    public String getShop_id() {
        return this.shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getTerminal_id() {
        return this.terminal_id;
    }

    public void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }

    public String getAgent_uid() {
        return this.agent_uid;
    }

    public void setAgent_uid(String agent_uid) {
        this.agent_uid = agent_uid;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaytime() {
        return this.paytime;
    }

    public void setPaytime(String paytime) {
        this.paytime = paytime;
    }

    public String getAddtime() {
        return this.addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getUptime() {
        return this.uptime;
    }

    public void setUptime(String uptime) {
        this.uptime = uptime;
    }

    public String getExternal_order_no() {
        return this.external_order_no;
    }

    public void setExternal_order_no(String external_order_no) {
        this.external_order_no = external_order_no;
    }

    public String getWorkmen_id() {
        return this.workmen_id;
    }

    public void setWorkmen_id(String workmen_id) {
        this.workmen_id = workmen_id;
    }

    @Override
    public String toString() {
        return "PayOrder{" +
                "id=" + id +
                ", out_trade_no='" + out_trade_no + '\'' +
                ", notify_count='" + notify_count + '\'' +
                ", notify_url='" + notify_url + '\'' +
                ", rqcode_url='" + rqcode_url + '\'' +
                ", body='" + body + '\'' +
                ", uid='" + uid + '\'' +
                ", trade_no='" + trade_no + '\'' +
                ", service='" + service + '\'' +
                ", product_id='" + product_id + '\'' +
                ", pay_type='" + pay_type + '\'' +
                ", pay_name='" + pay_name + '\'' +
                ", total_amount='" + total_amount + '\'' +
                ", discountable_amount='" + discountable_amount + '\'' +
                ", pay_amount='" + pay_amount + '\'' +
                ", pay_uid='" + pay_uid + '\'' +
                ", shop_id='" + shop_id + '\'' +
                ", terminal_id='" + terminal_id + '\'' +
                ", agent_uid='" + agent_uid + '\'' +
                ", status='" + status + '\'' +
                ", paytime='" + paytime + '\'' +
                ", addtime='" + addtime + '\'' +
                ", uptime='" + uptime + '\'' +
                ", external_order_no='" + external_order_no + '\'' +
                ", workmen_id='" + workmen_id + '\'' +
                '}';
    }

    public String getRqcode_url() {
        return this.rqcode_url;
    }

    public void setRqcode_url(String rqcode_url) {
        this.rqcode_url = rqcode_url;
    }
}
